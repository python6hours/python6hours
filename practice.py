'''
animal = "고양이"
name = "해피"
age = 4
hobby = "낮잠"
is_adult = age >= 3

print("우리집 " + animal + " 이름은 " + name + "에요")
hobby = "공놀이"
#print(name + "는 " + str(age) +"살이며, " + hobby + "을 아주 좋아해요")
print(name, "는", age, "살이며,", hobby, "을 아주 좋아해요")
print(name + "는 어른일까요? " + str(is_adult))


여기 부터는 계속 주석입니다.

주석이라구요
'''

# station = "인천공항"
# print(station, "행 열차가 들어오고 있습니다.")

# print(2**3)
# print(5%3)
# print(10//3)
# print(not(3+4 != 78))

# print((3>10) or (31<5))

from random import *
# print(int(random()*45+1))
# print(int(random()*45+1))
# print(int(random()*45+1))
# print(int(random()*45+1))
# print(int(random()*45+1))
# print(int(random()*45+1))

# print(randrange(1, 46))
# print(randrange(1, 46))
# print(randrange(1, 46))
# print(randrange(1, 46))
# print(randrange(1, 46))
# print(randrange(1, 46))

# print(randint(1,45))
# print(randint(1,45))
# print(randint(1,45))
# print(randint(1,45))
# print(randint(1,45))
# print(randint(1,45))

date = randint(4, 28)
print("오프라인 스터디 모임 날짜는 매월", date, "일로 선정되었습니다.")